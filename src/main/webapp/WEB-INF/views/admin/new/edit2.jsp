<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/common/taglib.jsp"%>
<c:url var="APIurl" value="/api/new"/>
<c:url var ="createEditNewURL" value="/quan-tri/bai-viet/chinh-sua"/>
<c:url var ="NewURL" value="/quan-tri/bai-viet/danh-sach"/>
<html>
<head>
    <title>
		<c:if test="${not empty model.id}">
           	Chỉnh sửa bài viết
        </c:if>
        <c:if test="${empty model.id}">
            Thêm bài viết
        </c:if>
    </title>
</head>
<body>
<div class="main-content">
    <div class="main-content-inner">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">Trang chủ</a>
                </li>
                <li class="active">
					<c:if test="${not empty model.id}">
                 		Chỉnh sửa bài viết
                	</c:if>
                	<c:if test="${empty model.id}">
                 		Thêm bài viết
                 	</c:if>
                </li>
            </ul><!-- /.breadcrumb -->
        </div>
        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                		<c:if test="${not empty messageResponse}">
							<div class="alert alert-${alert}">
  									${messageResponse}
							</div>
						</c:if>
                        <form id="formSubmit" action="<c:url value ='/quan-tri/bai-viet/chinh-sua' />" method="post" accept-charset="UTF-8">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right">Thể loại</label>
                                <div class="col-sm-9">
									<select class="form-control" id="categoryCode" name="categoryCode">
                                       <option value="">Chọn thể loại bài viết</option>
                                       <c:forEach var="item" items="${categories}">
                                       		<option value="${item.code}" <c:if test="${item.code == model.categoryCode}">selected="selected"</c:if>>${item.name}</option>
                                       </c:forEach>
                                    </select>
                                </div>
                            </div>
                            <br/>
                            <br/>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right">Tiêu đề</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="title" name="title" value="${model.title}"/>
                                </div>
                            </div>
                            <br/>
                            <br/>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right">Hình đại diện</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="thumbnail" name="thumbnail" value="${model.thumbnail}"/>
                                </div>
                            </div>
                            <br/>
                            <br/>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right">Mô tả ngắn</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="shortDescription" name="shortDescription" value="${model.shortDescription}"/>
                                </div>
                            </div>
                            <br/>
                            <br/>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right">Nội dung</label>
                                <div class="col-sm-9">                                 
                                   <textarea rows="" cols="" id="content" name="content" style="width: 820px;height: 175px">${model.content}</textarea>
                                </div>
                            </div>
                            <br/>
                            <br/>
                           <%--  <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="button" class="btn btn-white btn-warning btn-bold" value="<c:if test="${not empty model.id}">Cập nhật bài viết</c:if>
                                    																	   <c:if test="${empty model.id}">Thêm bài viết</c:if>" id="btnAddOrUpdateNew"/>
                                </div>
                            </div> --%>
                           <input type="hidden" value="${model.id}" id="id" name="id"/>
                           <input type="submit" value="Submit" class="btn btn-white btn-warning btn-bold"/>
                     </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
	var editor = '';
	$(document).ready(function(){
		editor = CKEDITOR.replace( 'content');
	});	
</script>
</body>
</html>
