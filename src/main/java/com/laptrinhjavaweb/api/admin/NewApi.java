package com.laptrinhjavaweb.api.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.laptrinhjavaweb.dto.NewDTO;
import com.laptrinhjavaweb.service.INewService;

@RestController(value = "newApiOfAdmin")
public class NewApi {
	
	@Autowired
	private INewService newService;

	@PostMapping(value = "/api/new")
	public NewDTO createNew(@RequestBody NewDTO newDto) {
		return newService.save(newDto);
	}

	@PutMapping(value = "/api/new")
	public NewDTO updateNew(@RequestBody NewDTO newDto) {
		return newService.save(newDto);
	}

	@DeleteMapping(value = "/api/new")
	public void deleteNew(@RequestBody Long[] ids) {
		newService.delete(ids);
	}
}
