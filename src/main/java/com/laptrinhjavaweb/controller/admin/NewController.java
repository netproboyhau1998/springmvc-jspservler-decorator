package com.laptrinhjavaweb.controller.admin;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.laptrinhjavaweb.dto.NewDTO;
import com.laptrinhjavaweb.service.ICategoryService;
import com.laptrinhjavaweb.service.INewService;
import com.laptrinhjavaweb.util.MessageUtils;

@Controller(value = "newControllerOfAdmin")
@RequestMapping(value = "/quan-tri")
public class NewController {

	@Autowired
	private INewService newService;
	
	@Autowired
	private ICategoryService categoryService;
	
	@Autowired
	private MessageUtils messageUtils;
	
	@RequestMapping(value = "/bai-viet/danh-sach", method = RequestMethod.GET)
	public ModelAndView showList(@RequestParam(name = "page", defaultValue = "0") int page, @RequestParam(name = "limit", defaultValue = "0") int limit, HttpServletRequest request) {
		if(page == 0 && limit == 0) {
			page = 1;
			limit = 2;
		}
		NewDTO newDTO = new NewDTO();
		newDTO.setPage(page);
		Pageable pageable = new PageRequest(page - 1, limit);
		List<NewDTO> news = newService.findAll(pageable);
		newDTO.setListResult(news);
		int totalItem = newService.totalItem();
		int totalPage = (int) Math.ceil((double) totalItem / limit);
		newDTO.setTotalItem(totalItem);
		newDTO.setTotalPage(totalPage);
		ModelAndView mav = new ModelAndView("admin/new/list");
		String message = request.getParameter("message");
		if(message != null) {
			Map<String, String> messageMap = messageUtils.getMessage(message);
			mav.addObject("messageResponse", messageMap.get("messageResponse"));
			mav.addObject("alert", messageMap.get("alert"));
		}
		mav.addObject("model", newDTO);
		return mav;
	}

	@RequestMapping(value = "/bai-viet/chinh-sua", method = RequestMethod.GET)
	public ModelAndView createOrEditNew(@RequestParam(value = "id", required = false) Long id, HttpServletRequest request) {
		ModelAndView mav = new ModelAndView("admin/new/edit2");
		NewDTO newDTO = new NewDTO();
		if (id != null) {
			newDTO = newService.findById(id);
		}
		String message = request.getParameter("message");
		if(message != null) {
			Map<String, String> messageMap = messageUtils.getMessage(message);
			mav.addObject("messageResponse", messageMap.get("messageResponse"));
			mav.addObject("alert", messageMap.get("alert"));
		}
		mav.addObject("categories", categoryService.findAll());
		mav.addObject("model", newDTO);
		return mav;
	}
	
	//sử dụng edit2.jsp
	@RequestMapping(value = "/bai-viet/chinh-sua", method = RequestMethod.POST)
	public String createOrEditNew(NewDTO newDTO) {
		try {
			newDTO	= newService.save(newDTO);
			return "redirect:/quan-tri/bai-viet/chinh-sua?id="+newDTO.getId()+"&message=insert_success";
		} catch (Exception e) {
			return "redirect:/quan-tri/bai-viet/danh-sach?page=1&limit=2&message=error_system";
		}
	}
}
