package com.laptrinhjavaweb.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.laptrinhjavaweb.converter.NewConverter;
import com.laptrinhjavaweb.dto.NewDTO;
import com.laptrinhjavaweb.entity.CategoryEntity;
import com.laptrinhjavaweb.entity.NewEntity;
import com.laptrinhjavaweb.repository.CategoryRepository;
import com.laptrinhjavaweb.repository.NewRepository;
import com.laptrinhjavaweb.service.INewService;

@Service
public class NewService implements INewService {

	@Autowired
	private NewRepository newRep;

	@Autowired
	private NewConverter newConverter;

	@Autowired
	private CategoryRepository categoryRep;

	@Override
	public List<NewDTO> findAll(Pageable pageable) {
		List<NewEntity> entities = newRep.findAll(pageable).getContent();
		List<NewDTO> newDTOs = new ArrayList<>();
		for (NewEntity entity : entities) {
			NewDTO newDTO = newConverter.toDto(entity);
			newDTOs.add(newDTO);
		}
		return newDTOs;
	}

	@Override
	public int totalItem() {
		return (int) newRep.count();
	}

	@Override
	public NewDTO findById(Long id) {
		NewEntity newEntity = newRep.findOne(id);
		return newConverter.toDto(newEntity);
	}

	@Override
	@Transactional
	public NewDTO save(NewDTO dto) {
		CategoryEntity categoryEntity = categoryRep.findOneByCode(dto.getCategoryCode());
		NewEntity newEntity = new NewEntity();
		if (dto.getId() != null) {
			NewEntity oldNew = newRep.findOne(dto.getId());
			oldNew.setCategory(categoryEntity);
			newEntity = newConverter.toEntity(oldNew, dto);
		}else {
			newEntity = newConverter.toEntity(dto);
			newEntity.setCategory(categoryEntity);
		}
		return newConverter.toDto(newRep.save(newEntity));
	}

	@Override
	public void delete(Long[] ids) {
		for(Long id : ids) {
			newRep.delete(id);
		}
	}
}
